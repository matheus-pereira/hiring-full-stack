<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class UserController extends Controller
{
    private $client;

    function __construct()
    {
        $this->client = new Client();
    }

    private function request($url)
    {
        $response = $this->client->get($url);
        return json_decode($response->getBody());
    }

    public function getUser(Request $request, $userName)
    {
        $user = $this->request('https://api.github.com/users/' . $userName);
        $response = [
            'login' => $user->login,
            'name' => $user->name,
            'bio' => $user->bio,
            'avatar_url' => $user->avatar_url,
            'html_url' => $user->html_url,
            'repositories' => [],
            'followers' => []
        ];

        $repositories = $this->request($user->repos_url);
        foreach ($repositories as $repository)
        {
            $response['repositories'][] = [
                'name' => $repository->name,
                'description' => $repository->description,
                'html_url' => $repository->html_url,
                'clone_url' => $repository->clone_url
            ];
        }
        
        $followers = $this->request($user->followers_url);
        foreach ($followers as $follower)
        {
            $response['followers'][] = [
                'login' => $follower->login,
                'avatar_url' => $follower->avatar_url
            ];
        }

        return response()->json($response);
    }
}
