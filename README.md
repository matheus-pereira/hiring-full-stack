# GitHub User View

Hey there!

In this project you will find an Angular application that receives a github username account, makes a search and return data from [GitHub API](https://developer.github.com/v3/).

Also, I build a Laravel back-end to receive the username and return a custom github user view.

## Requirements

- Laravel 5.4
- Angular 4
- Docker
- Git
- Linux based-system

## Running the project

Open a terminal and execute `git clone https://gitlab.com/matheus-pereira/hiring-full-stack.git`, then enter in the created folder with `cd hiring-full-stack`

Install the dependencies with `composer install`, run a local server with `php artisan serve` and you'll be able to access the application in your browser ar `localhost:8000`

If you want to edit the Angular source code, open another terminal in the same directory, enter in the Angular folder with `cd angular` and install the dependencies with `npm install`

You can run another developement server with `ng serve`, but you still need the Laravel server running

To apply the changes to Laravel, run `npm run deploy`, a script will build the application and put the output there

Also, you can create a docker image from the Laravel server, I get a Dockerfile for you

#### That's it, see ya!