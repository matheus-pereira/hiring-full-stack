import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { User } from './user.interface';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class UserResolver implements Resolve<Observable<User>> {
    
    constructor(private userService: UserService) { }

    resolve(route: ActivatedRouteSnapshot) {
        const userName = route.params.userName;
        return this.userService.getUser(userName);
    }

}