export interface Repository {
    name:string;
    description:string;
    html_url:string;
    clone_url:string;
}