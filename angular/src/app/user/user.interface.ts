import { Repository } from './repository.interface';
import { Follower } from './follower.interface';

export interface User {
    login:string;
    name:string;
    bio:string;
    avatar_url:string;
    html_url:string;
    repositories: Array<Repository>;
    followers: Array<Follower>;
}