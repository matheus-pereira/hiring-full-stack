import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from './user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  getUser(userName:string) {
    return this.http.get<User>(`${environment.apiUrl}/api/user/${userName}`);
  }

  searchUser(userName:string) {
    return this.http.get<any>(`https://api.github.com/search/users?q=${userName} in:login type:user&per_page=15`);
  }
  
}
