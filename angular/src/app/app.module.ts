import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavbarModule } from './navbar/navbar.module';
import { UserSearchModule } from './user-search/user-search.module';
import { AppRoutingModule } from './app.routing.module';
import { UserViewModule } from './user-view/user-view.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NavbarModule,
    UserSearchModule,
    UserViewModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
