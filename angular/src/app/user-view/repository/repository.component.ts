import { Component, Input } from '@angular/core';
import { Repository } from '../../user/repository.interface';

@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html'
})
export class RepositoryComponent {

  @Input() repository: Repository[];

  copyToClipboard(url) {
    
  }

}
