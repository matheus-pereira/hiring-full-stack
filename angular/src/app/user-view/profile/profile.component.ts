import { Component, Input } from '@angular/core';
import { User } from '../../user/user.interface';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent {

  @Input() user: User;

}
