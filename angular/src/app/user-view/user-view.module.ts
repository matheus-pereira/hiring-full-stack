import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ClipboardModule } from 'ngx-clipboard';
import { NavbarModule } from '../navbar/navbar.module';
import { ProfileComponent } from './profile/profile.component';
import { RepositoryComponent } from './repository/repository.component';
import { FollowerComponent } from './follower/follower.component';
import { UserViewComponent } from './user-view.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ClipboardModule,
    NavbarModule
  ],
  declarations: [
    UserViewComponent,
    ProfileComponent,
    RepositoryComponent,
    FollowerComponent
  ],
  exports: [
    UserViewComponent
  ]
})
export class UserViewModule { }
