import { Component, Input } from '@angular/core';
import { Follower } from '../../user/follower.interface';

@Component({
  selector: 'app-follower',
  templateUrl: './follower.component.html',
  styleUrls: ['./follower.component.css']
})
export class FollowerComponent {

  @Input() follower: Follower[];

}
