import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { User } from '../user/user.interface';
import { UserService } from '../user/user.service';

@Component({
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.css']
})
export class UserSearchComponent implements OnInit {

  debounce: Subject<string> = new Subject<string>();
  users: User[] = [];
  notFound: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.debounce
      .pipe(debounceTime(500))
      .subscribe(userName => userName && this.search(userName));
  }

  search(userName) {
    this.userService
      .searchUser(userName)
      .subscribe((response) => {
        this.users = response.items;
        if (!response.items.length) this.notFound = true;
      });
  }

  ngOnDestroy(): void {
    this.debounce.unsubscribe();
  }

}
