import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavbarModule } from '../navbar/navbar.module';
import { UserSearchComponent } from './user-search.component';

@NgModule({
  declarations: [
    UserSearchComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NavbarModule
  ],
  exports: [
      UserSearchComponent
  ]
})
export class UserSearchModule { }
